<?php
namespace Tests\Unit;

use Basecode\Ingredient;

class IngredientTest extends \PHPUnit_Framework_TestCase
{

	public function testIsStaleForFreshItems()
	{
		$ingredient = new Ingredient('Ham', 10, 'slices');
		$ingredient->setUseBy(date('d/m/Y', strtotime('next week')));
		$actual = $ingredient->isStale();
		$this->assertEquals($actual, false);
	}

	public function testIsStaleForStaleItems()
	{
		$ingredient = new Ingredient('Ham', 10, 'slices');
		$ingredient->setUseBy(date('d/m/Y', strtotime('last week')));
		$actual = $ingredient->isStale();
		$this->assertEquals($actual, true);
	}

	/**
	 * @dataProvider setUnitDataProvider
	 */
	public function testSetUnitValidates($name, $quantity, $unit, $exception)
	{
		if ($exception) {
			$this->setExpectedException('Exception');
		}
		$ingredient = new Ingredient($name, $quantity, $unit);
	}

	public function setUnitDataProvider()
	{
		return array(
			array('Bannana', 10, 'of', false),
			array('Salt', 4, 'grams', false),
			array('Milk', 5000, 'ml', false),
			array('Bacon', 12, 'slices', false),
			array('Tim Tams', 3, 'packets', true)
		);
	}

}