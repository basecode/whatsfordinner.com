<?php
namespace Basecode;

use Basecode\Ingredient;

class Recipe
{
	protected $name;

	protected $ingredients = array();

	public function __construct($name)
	{
		$this->name = trim($name);
	}

	public function getName()
	{
		return $this->name;
	}

	public function setIngredient(Ingredient $ingredient)
	{
		$this->ingredients[$ingredient->getName()] = $ingredient;
	}

	public function getIngredients()
	{
		return $this->ingredients;
	}

	public function contains(Ingredient $ingredient)
	{
        return array_key_exists($ingredient->getName(), $this->ingredients);
	}

}